# This project was moved to:
```
https://github.com/allucardster/ws-xample
```

This means:
- Any new modification or update will be performed in github repo
- This will no longer supported

What should do now?
===================

You got two options:

1. Update the git origin to github repo [doing something like this](https://stackoverflow.com/a/10904450)
2. Delete your local "ws-xample" copy and then clone from github repo

*Note*: The first option it's the best ;)

Contributors
============

- Richard Melo [Twitter](https://twitter.com/allucardster), [Linkedin](https://www.linkedin.com/in/richardmelo)
